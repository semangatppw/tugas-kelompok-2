**Link HerokuApp :**
http://tugaskelompok-pertama.herokuapp.com

Anggota kelompok 
1. Aulia Radityatama Suhendra (1906399865)
2. Fareeha Nisa Zayda Azeeza (1906399644)
3. Fariz Wahyuzan Dwitilas (1906399511)
4. Sultan Daffa Nusantara (1906399581)
5. Ian Andersen Ng (1906400280)


**Cerita aplikasi yang diajukan serta kebermanfaatannya**
Kami membuat website yang menyediakan berbagai informasi tentang COVID-19 
yang sedang mewabah sekarang. Kami menyediakan hotline . Kami juga menyediakan 
form yang dapat diisi pengunjung jika mereka menginginkan bantuan. 
Website kami menyediakan peta indonesia yang memperlihatkan zona-zona yang 
mempunyai persentase penularan yang tinggi 

**Daftar fitur yang akan diimplementasikan**
- Landing Page/Home: Form Contact Us untuk menjangkau mereka yang membutuhkan pertolongan
- Kisah COVID: Memberikan user wadah untuk bercerita mengenai apa saja keluhan mereka selama masa pandemi
- Feedback: Memberikan user wadah untuk memberikan feedback terhadap website
- Volunteer: Memberikan user kesempatan untuk menjadi volunteer bagi mereka yang terdampak Covid-19
- Donasi: Memberikan user wadah untuk melakukan donasi untuk membantu mereka yang terdampak Covid-19
