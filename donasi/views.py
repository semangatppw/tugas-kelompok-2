from django.shortcuts import render, redirect
from .models import Donasi, Form_Donasi
from django.db.models import Sum
from django.http.response import JsonResponse

def donasi(request):
    return render(request, 'donasi.html')

def ajax_donasi(request):
    count = Donasi.objects.all().count()
    if( count > 0):
        nominals = Donasi.objects.aggregate(total_donasi = Sum('nominal'))
    else:
        nominals = {"total_donasi" : 0 }
    return JsonResponse(nominals)

def tambah_donasi(request):
    form = Form_Donasi()
    return render (request, "form.html", {"form":form})

def ajax_form(request):
    if request.method == 'POST':
        ajax_tabel = {"nama":request.POST.get('nama'), "nomor":request.POST.get('nomor'), "nominal":request.POST.get('nominal')}
        post = Form_Donasi(ajax_tabel)
        if post.is_valid():
            post.save()
        else:
            data = {
                'response':"false"
            }
            return JsonResponse(data)
    return redirect("Donasi:donasi")

