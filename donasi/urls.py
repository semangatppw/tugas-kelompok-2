from django.urls import path
from .views import donasi, tambah_donasi, ajax_donasi, ajax_form

app_name = "Donasi"

urlpatterns = [
    path('', donasi, name="donasi"),
    path('form/', tambah_donasi, name='form'),
    path('form/ajax/', ajax_form, name='ajaxForm'),
    path('ajax/', ajax_donasi, name='ajax')


]