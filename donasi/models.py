from django.db import models
from django.forms import ModelForm
from django.forms import widgets, TextInput, Textarea


class Donasi (models.Model):
    nama = models.CharField("Nama Donatur", max_length=100, help_text="*Nama lengkap atau nama panggilan. Contoh: Rizky Fathur atau Rizky")
    nomor = models.PositiveBigIntegerField("Nomor Rekening", help_text="*Contoh: 731xxxxxxx")
    nominal = models.PositiveBigIntegerField("Nominal Donasi", help_text="*Contoh: 44000")

class Form_Donasi (ModelForm):
    class Meta :
        model = Donasi
        fields = ["nama", "nomor", "nominal"]
        widgets = {
            'nama': TextInput(attrs={'id': 'data-nama', 'required': True}),
            'nomor': TextInput(attrs={'id': 'data-nomor', 'required': True}),
            'nominal': TextInput(attrs={'id': 'data-nominal', 'required': True}),
        }

# Create your models here.
