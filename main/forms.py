from django import forms
from .models import feedback

class feedbackform(forms.ModelForm):
    
    class Meta:
        model = feedback
        fields = ('nama', 'feedback')
    
    '''
    nama = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Masukkan namamu',
        'type' : 'text',
        'required' : True
    }))
    feedback = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Masukkan pendapatmu',
        'type' : 'text',
        'required' : True
    }))
    '''