from django.shortcuts import render
from . import forms, models
from django.http import JsonResponse


def feedback_view(request):
    formku = forms.feedbackform(request.POST)
    data = {}

    if (request.is_ajax()):
        if(formku.is_valid()):
            formku.save()
            data['nama'] = formku.cleaned_data.get('nama')
            data['feedback'] = formku.cleaned_data.get('feedback')
            data['jumlah'] =  models.feedback.objects.count()
            return JsonResponse(data)
    
    '''
    if(request.method == "POST"):
        formku = forms.feedbackform(request.POST)
        if(formku.is_valid()):
            model_feedback = models.feedback()
            model_feedback.nama = formku.cleaned_data["nama"]
            model_feedback.feedback = formku.cleaned_data["feedback"]
            model_feedback.save() '''
        
    
    jumlah_input = models.feedback.objects.count()
    context = {
        'formulir' : formku,
        'jumlah_feedback' : jumlah_input
    }
    return render(request, 'main/home.html', context)