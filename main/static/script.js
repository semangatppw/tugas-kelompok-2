
// ubah semua input menjadi uppercase
$(document).ready(function () {  
    $("input[type=text]").keyup(function () {  
        $(this).val($(this).val().toLowerCase());  
    });  
});  

// alert saat form di submit 
$( "#formku" ).submit(function( event ) {
    alert( "Terima kasih telah memberi feedback" );
    event.preventDefault();
});

const alertBox = document.getElementById('alert-box')
const jumlahBox = document.getElementById('jumlah_feedback')
const nama = document.getElementById('id_nama')                 // mengambil nama dari form 
const feedback = document.getElementById('id_feedback')         // mengambil feedback dari form 

const csrf = document.getElementsByName('csrfmiddlewaretoken') // mengambil csrf dari form 
const url = ''                                                 // url untuk post
const form = document.getElementById('formku')


// method untuk alert response cek response 
const handleAlerts = (type,text) => 
    alertBox.innerHTML = `<div class="alert alert-${type}" role="alert">
                            ${text}</div>` 

//method untuk mengisi jumlah ke dalam html 
const handleJumlah = (text) =>
    jumlahBox.innerHTML = text


// tambahkan listener untuk form 
form.addEventListener('submit', e=>{

    e.preventDefault();
    const dataformku = new FormData(); // buat data untuk form yang baru lalu masukkan berbagai input 
    dataformku.append('csrfmiddlewaretoken', csrf[0].value)
    dataformku.append('nama',nama.value)
    dataformku.append('feedback',feedback.value)


    $.ajax({
        type: 'POST',
        url: url,
        enctype: 'multipart/form-data',
        data: dataformku,
        success: function(response){ //jika sukses 
            console.log(response) //response di console
            
            // buat alert
            const suksesteks = 'Successfully saved feedback '+ response.nama
            handleAlerts('success', suksesteks)
            
            //gunakan method untuk mengubah jumlah di html 
            handleJumlah(response.jumlah)
          
            // reset formnya 
            setTimeout(()=>{
                alertBox.innerHTML = ""
                nama.value = ""
                feedback.value = ""
            }, 5000)
        },
        //error: function(req, err){ console.log('message : ' + err); },
        error: function(xhr, textStatus, error) {
            console.log('response: ' + xhr.responseText);
            console.log('status: ' + xhr.statusText);
            console.log('status: ' + textStatus);
            console.log(error);
        },
       
        cache: false,
        contentType: false,
        processData: false,
    })
})


//toggle untuk darkmode
$(".switch").click(function() {
    console.log("berhasil");
    $("#canvas-wrapper").toggleClass("dark-mode");
});


