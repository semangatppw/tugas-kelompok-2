//Ajax call: masukin dan ngitung data secara live 
const form = document.getElementById("post-form")
const alertBox = document.getElementById('alert-box')

const nama = document.getElementById("id_nama")
const cerita = document.getElementById("id_cerita")

const csrf = document.getElementsByName('csrfmiddlewaretoken')

const url = ""

const jmlKisah = document.getElementById('total_kisah')
const handlerJml = (text) => jmlKisah.innerHTML = text

const handleAlerts = (type, text) =>{
    alertBox.innerHTML = `<div class="alert alert-${type}" role="alert">${text}</div>`
}

form.addEventListener('submit', e=>{
    e.preventDefault()

    const fd = new FormData()
    fd.append('csrfmiddlewaretoken', csrf[0].value)
    fd.append('nama', nama.value)
    fd.append('cerita', cerita.value)

    $.ajax({
        type:'POST',
        url:url,
        enctype:'multipart/form-data',
        data:fd,
        success:function(response){
            console.log(response)
            const sText = `successfully saved ${response.nama}`
            handleAlerts('success', sText)
            handlerJml(response.total)
            setTimeout(()=>{
                alertBox.innerHTML = ""
                nama.value = ""
                cerita.value = ""
            }, 3000)
        },
        error: function(error){
            console.log(error)
            handleAlerts('bahaya', 'oops..something went wrong')
        },
        cache: false,
        contentType: false,
        processData: false,
    })
})




//Search box
$(document).ready(function(){
    $("#myInput").keyup(function(){
 
        // Retrieve the input field text and reset the count to zero
        var filter = $(this).val(), count = 0;
 
        // Loop through the comment list
        $("#myUL .buatbuat").each(function(){
 
            // If the list item does not contain the text phrase fade it out
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).fadeOut();
 
            // Show the list item if the phrase matches and increase the count by 1
            } else {
                $(this).show();
                count++;
            }
        });
    });
});

//Accordion
$('.card-header').click(function(e) {
    e.preventDefault();

  let $this = $(this);

  if ($this.next().hasClass('show')) {
      $this.next().removeClass('show');
      $this.next().slideUp(350);
  } else {
      $this.parent().parent().find('.card bg-light mb-3 .card-body').removeClass('show');
      $this.parent().parent().find('.card bg-light mb-3 .card-body').slideUp(350);
      $this.next().toggleClass('show');
      $this.next().slideToggle(350);
  }
});


 //Dark mode
 function myFunction() {
    var element = document.body;
    element.classList.toggle("dark-mode");
 }

 //Ganti warna card-header (mouseenter)
 $(document).ready(function(){
    $(".jml").dblclick(function(){
      $(this).hide();
    });
  });
