from django.shortcuts import render,redirect
from .models import Kisah as kisahh
from .forms import KisahForm
from django.http import JsonResponse


# Create your views here.
def submit_kisah(request):
    form = KisahForm(request.POST or None, request.FILES or None)
    data={}
    items = kisahh.objects.all()
    jml = kisahh.objects.count()
    if request.is_ajax():
        if form.is_valid():
            form.save()
            data['nama'] = form.cleaned_data.get('nama')
            data['total'] =  kisahh.objects.count()
            return JsonResponse(data)

    context = {
        "form" : form,
        "items" : items,
        'total_kisah' : jml,
    }
    return render(request, "formkisah.html", context)


