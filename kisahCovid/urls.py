from django.urls import path
from .views import submit_kisah

app_name = 'kisahCovid'

urlpatterns = [
    path('', submit_kisah, name = 'kisahcovid'),
]