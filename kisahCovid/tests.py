from django.test import TestCase, Client
from django.urls import resolve
from .views import submit_kisah
from .models import Kisah
from .forms import KisahForm
from django.contrib.auth.models import User

# Create your tests here.
class TestHalamanKisah(TestCase):
    def test_kisah_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_using_func(self):
        found = resolve('/kisahcovid/')
        self.assertEqual(found.func, submit_kisah)
    
    def test_using_home_html(self):
        response = Client().get('/kisahcovid/')
        self.assertTemplateUsed(response, 'formkisah.html')

    def test_header_ada(self):
        response = Client().get('/kisahcovid/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Tuliskan kisahmu", html_kembalian)
        self.assertTemplateUsed(response, 'formkisah.html')

    def test_title_ada(self):
        response = Client().get('/kisahcovid/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Kisah Covid", html_kembalian)
        self.assertTemplateUsed(response, 'formkisah.html')

    def test_get_kisah(self):
        response = Client().get('/kisahcovid/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Tuliskan kisahmu", html_kembalian)

    def test_apakah_model_kisah_bekerja(self):
	    Kisah.objects.create(nama="pewe", cerita="Ini cerita")
	    hitung_berapa_objectnya = Kisah.objects.all().count()
	    self.assertEquals(hitung_berapa_objectnya, 1)

    def test_post_kisah_di_form(self):
        datas = {'nama':'Ini nama','cerita':'Ini cerita'}
        form = KisahForm(data=datas)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['nama'], 'Ini nama')
        self.assertEqual(form.cleaned_data['cerita'], 'Ini cerita')

    def test_konten_ada_sebelom_login(self):
        response = Client().get('/kisahcovid/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("di sini </a> untuk menulis kisahmu.</p>", html_kembalian)
        self.assertTemplateUsed(response, 'formkisah.html')

    def test_login(self):
        user = User.objects.create(username='testuser')
        user.set_password('t35tt35t')
        user.save()
        c = Client()
        c.login(username='testuser', password='t35tt35t')