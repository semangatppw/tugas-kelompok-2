from django import forms
from .models import Kisah

class KisahForm(forms.ModelForm):

    class Meta:
        model = Kisah
        fields = ('nama','cerita')