from django.db import models

# Create your models here.
class Kisah(models.Model):
    nama = models.CharField(max_length = 100)
    cerita = models.TextField(max_length = 500)

    def __str__(self):
        return str(self.nama)