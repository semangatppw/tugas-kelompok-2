from . import forms, models

from django.test import TestCase , Client
from django.http import HttpRequest
from django.apps import apps
from django.urls import resolve, reverse
from django.contrib.auth.models import User
from .helper import kasus_provinsi_helper

# apps
from landingpage.apps import LandingpageConfig

# views
from . import views

# Models
from .models import KasusProvinsi, ContactUs

class GeneralTest(TestCase):
  def test_does_pipeline_work(self):
    self.assertEquals(1, 1)

  def test_status(self):
    response = Client().get('')
    self.assertEqual(response.status_code, 200)
  
  def setUp(self):
    self.client = Client()
  
  def test_app(self):
    self.assertEqual(LandingpageConfig.name, 'landingpage')
        

class TestURLS(TestCase):
  # Testing urls
  def test_base_url_exists(self):
    response = Client().get('')
    self.assertEquals(response.status_code, 200)

  def test_base_url_template(self):
    response = self.client.get('')
    self.assertTemplateUsed(response, 'index.html')

  def setUp(self):
    self.info_page = reverse('landingpage:index')
  
  def test_table_data_exists(self):
    c = Client()
    response = c.get(self.info_page)
    self.assertEqual(response.status_code, 200)
    self.assertTemplateUsed(response, 'index.html')
    self.assertContains(response, 'Kasus COVID-19 di Indonesia')


class TestViews(TestCase):
  # Testing views
  def test_index_view_response(self):
    response = views.index(HttpRequest())
    self.assertEquals(response.status_code, 200)

  def test_using_func(self):
      found = resolve('/')
      self.assertEqual(found.func, views.index)

  # Test Autentifikasi
  def test_user_not_logged_in(self):
      response = self.client.get('/')
      result = response.content.decode("utf8")
      self.assertIn("untuk mengisi data.", result)
  

class TestModels(TestCase):
  # Testing models
  def test_create_contactus(self):
    new_response = ContactUs(name='name', email='email', phone='phone', age='age', description='description')
    new_response.save()
    self.assertEquals(ContactUs.objects.all().count(), 1)

class ModelsKasusProvinsiTest(TestCase):
    def setUp(self):
        KasusProvinsi.objects.create(
            provinsi = "DKI Jakarta",
            kasus_positif = 50,
            kasus_sembuh = 1,
            kasus_meninggal = 2
        )

    def test_models_true(self):
            kasus_jakarta = KasusProvinsi.objects.get(provinsi = "DKI Jakarta")
            self.assertEqual(KasusProvinsi.objects.count(), 1)
            self.assertEqual(str(kasus_jakarta), "DKI Jakarta")
            self.assertEqual(kasus_jakarta.kasus_positif, 50)
            self.assertEqual(kasus_jakarta.kasus_sembuh, 1)
            self.assertEqual(kasus_jakarta.kasus_meninggal, 2)
            kasus_jakarta.delete()
            self.assertEqual(KasusProvinsi.objects.count(), 0)

class HelperFunctionTest(TestCase):
    def setUp(self):
        KasusProvinsi.objects.create(
            provinsi = "DKI Jakarta",
            kasus_positif = 50,
            kasus_sembuh = 1,
            kasus_meninggal = 2
        )

        self.data = [{"attributes":{"FID":11,"Kode_Provi":31,"Provinsi":"DKI Jakarta","Kasus_Posi":107229,"Kasus_Semb":95783,"Kasus_Meni":2288}}]
    
    def test_kasus_provinsi_helper(self):
      kasus_provinsi_helper(self.data)
      kasus_jakarta = KasusProvinsi.objects.get(
          provinsi = "DKI Jakarta"
      )
      self.assertEqual(kasus_jakarta.kasus_positif, 107229)
      self.assertEqual(kasus_jakarta.kasus_sembuh, 95783)
      self.assertEqual(kasus_jakarta.kasus_meninggal, 2288)

class TestForm(TestCase):
  #Testing forms
  def test_form_post(self):
      response_post = Client().post('', {'name':"Ian",'email':"ian@ui.ac.id", 'phone':"0123456789", 'age':"19", 'description':"Sakit"})
      counter = ContactUs.objects.all().count()
      self.assertEqual(response_post.status_code,200)
      self.assertEqual(counter, 0)

  
  
  





