from django.db import models

# Create your models here.

class ContactUs(models.Model):
  name = models.CharField(max_length=128)
  email = models.CharField(max_length=128)
  phone = models.CharField(max_length=128)
  age = models.CharField(max_length=128)
  description = models.TextField()

  def __str__(self):
        return self.name + ", " + self.age

class KasusProvinsi(models.Model):
    provinsi = models.CharField(max_length=50)
    kasus_positif = models.IntegerField()
    kasus_sembuh = models.IntegerField()
    kasus_meninggal = models.IntegerField()

    def __str__(self):
        return self.provinsi