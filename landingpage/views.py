import requests
from django.shortcuts import render
from django.http import JsonResponse

from .helper import kasus_provinsi_helper

# Models
from .models import KasusProvinsi, ContactUs

# Forms
from .forms import ContactForm

def index(request):
    response = requests.get('https://api.kawalcorona.com/indonesia/provinsi')
    data = response.json()
    kasus_provinsi_helper(data)
    data_semua_kasus = KasusProvinsi.objects.all()

    form = ContactForm()
    jumlah_input = ContactUs.objects.count()
    context = {
        'semua_data' : data_semua_kasus,
        'title' : 'Silahkan isi data',
        'form': form,
        'jumlah_input' : jumlah_input
    }
    return render(request, 'index.html', context)
   
def get_contact(request):
    json = list(ContactUs.objects.values())
    json.reverse()
    print(json)

    return JsonResponse({'data' : json}, safe=False)

def post_contact(request, name, email, phone, age, description):

    if (request.user.is_authenticated):
        form = ContactUs(name = name, email = email, phone = phone, age = age, description = description)   
        form.save()
    
    return get_contact(request)









