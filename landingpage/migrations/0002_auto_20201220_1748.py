# Generated by Django 3.1.2 on 2020-12-20 10:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('landingpage', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contactus',
            name='policy',
        ),
        migrations.AlterField(
            model_name='contactus',
            name='age',
            field=models.CharField(max_length=128),
        ),
        migrations.AlterField(
            model_name='contactus',
            name='description',
            field=models.CharField(max_length=500),
        ),
    ]
