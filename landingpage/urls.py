from django.urls import path

from . import views

app_name = 'landingpage'

urlpatterns = [
    path('', views.index, name='index'),
    path('get-contact/', views.get_contact, name='getcontact'),
    path('post-contact/<str:name>/<str:email>/<str:phone>/<str:age>/<str:description>/', views.post_contact),
    
]