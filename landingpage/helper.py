from .models import KasusProvinsi

def kasus_provinsi_helper(data):
    is_empty = KasusProvinsi.objects.count() == 0
    if is_empty:
        for objek in data:
            KasusProvinsi.objects.create(
                provinsi = objek["attributes"]["Provinsi"],
                kasus_positif = objek["attributes"]["Kasus_Posi"],
                kasus_sembuh = objek["attributes"]["Kasus_Semb"],
                kasus_meninggal = objek["attributes"]["Kasus_Meni"]
            )
    jakarta_object = KasusProvinsi.objects.get(provinsi = "DKI Jakarta")
    jakarta_object_json = data[0]["attributes"]
    is_latest = jakarta_object.kasus_positif == jakarta_object_json["Kasus_Posi"]
    if not is_latest:
        for objek in data:
            kasus_provinsi = KasusProvinsi.objects.get(
                provinsi = objek["attributes"]["Provinsi"]
            )
            kasus_provinsi.kasus_positif = objek["attributes"]["Kasus_Posi"]
            kasus_provinsi.kasus_sembuh = objek["attributes"]["Kasus_Semb"]
            kasus_provinsi.kasus_meninggal = objek["attributes"]["Kasus_Meni"]
            kasus_provinsi.save()