from django.contrib import admin
from django.db import models

from .models import ContactUs

# Register your models here.
admin.site.register(ContactUs)