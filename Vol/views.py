from django.shortcuts import render, get_object_or_404, redirect
from .models import RumahSakit, Volunteer
from .forms import FormRumahSakit, FormVolunteer
from django.http import HttpResponseRedirect
from django.http import JsonResponse

def home(request):
    tRumahSakit = RumahSakit.objects.all()
    data = {}
    form = FormRumahSakit(request.POST, request.FILES)

    # if (request.is_ajax()):
    #     form = FormRumahSakit(request.POST, request.FILES)
    #     if(form.is_valid()):
    #         form.save()
    #         data['nama'] = form.cleaned_data.get('nama_Rumah_Sakit')
    #         data['alamat'] = form.cleaned_data.get('alamat')
    #         data['telepon'] = form.cleaned_data.get('telepon')
    #         data['gambar'] = form.cleaned_data.get('gambar') 
    #         return JsonResponse(data)
    
    # if request.method == "POST":
    #     form = FormRumahSakit(request.POST, request.FILES)
    #     if form.is_valid():
    #         form.save()
    #         return redirect("/")

    context = {
        "rumahsakit" : tRumahSakit,
        "form" : form
    }
    return render(request, 'home.html', context)

def tambahRumahSakit(request,*args, **kwargs):
    namaRumahSakit = request.GET.get('nama_rumah_sakit ')
    alamatRumahSakit = request.GET.get('alamat ')
    teleponRumahSakit = request.GET.get('telepon ')
    gambarRumahSakit = request.GET.get('gambar ')


    RumahSakitBaru = RumahSakit.objects.create(nama_Rumah_Sakit = namaRumahSakit, alamat = alamatRumahSakit, telepon = teleponRumahSakit, gambar = gambarRumahSakit)
    
    data = []
    
    for dataBaru in RumahSakit.objects.all():
        dataJson = {}

        dataJson["nama"] = dataBaru.nama_Rumah_Sakit
        dataJson["alamat"] = dataBaru.alamat
        dataJson["telepon"] = dataBaru.telepon
        dataJson["gambar"] = dataBaru.gambar
        dataJson["jumlah"] = dataBaru.volunteer.objects.count()
        dataJson["volunteerRS"] = dataBaru.absolute_url()

        data.append(dataJson)

    return JsonResponse(data)
    

def volunteer(request, *args, **kwargs):
    form = FormVolunteer()

    if request.method == "POST":
        nama_Volunteer = request.POST.get("nama_Volunteer")
        KTP = request.POST.get("KTP")
        alasan = request.POST.get("alasan")
        volunteerDaftar = Volunteer.objects.create(nama_Volunteer = nama_Volunteer, KTP = KTP, alasan = alasan)
        rumahsakit = RumahSakit.objects.get(id = kwargs["id"])
        rumahsakit.volunteer.add(volunteerDaftar)
        return redirect("/volunteer/")
    
    
    context = {
        "form" : form,
    }

    return render(request, "tambahVolunteer.html", context)