from django.urls import path

from . import views

app_name = 'Vol'

urlpatterns = [
    path('', views.home, name='home'),
    path('tambah-rumah-sakit/', views.tambahRumahSakit, name='rumahsakit'),
    path('tambah-volunteer/<int:id>', views.volunteer, name='volunteer'),
]