from django.contrib import admin
from .models import RumahSakit, Volunteer

# Register your models here.
admin.site.register(RumahSakit)
admin.site.register(Volunteer)