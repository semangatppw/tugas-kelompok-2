from django.test import TestCase, Client
from django.urls import resolve, reverse
from .models import *
from .forms import *
from .views import loginPage, logoutUser, registerPage
from django.contrib.auth.models import User
from django.apps import apps
from accounts.apps import AccountsConfig

# Create your tests here.

class GeneralTest(TestCase):
  def test_does_pipeline_work(self):
    self.assertEquals(1, 1)

  def test_status(self):
    response = Client().get('/')
    self.assertEqual(response.status_code, 200)
  
  def setUp(self):
    self.client = Client()
    self.register = reverse('accounts:register')
    self.login = reverse('accounts:login')
  
  def test_app(self):
    self.assertEqual(AccountsConfig.name, 'accounts')

  #Test Login Page
  def test_login_exists(self):
    c = Client()
    response = c.get(self.login)
    self.assertEqual(response.status_code, 200)
    self.assertTemplateUsed(response, 'login.html')

  def test_title_login_ada(self):
    response = Client().get('/accounts/login/')
    html_kembalian = response.content.decode('utf8')
    self.assertIn("Login", html_kembalian)
    self.assertTemplateUsed(response, 'login.html')
  
  def test_get_login(self):
    response = Client().get('/accounts/login/')
    html_kembalian = response.content.decode('utf8')
    self.assertIn("LOGIN", html_kembalian)

  #Test Register Page
  def test_register_exists(self):
    c = Client()
    response = c.get(self.register)
    self.assertEqual(response.status_code, 200)
    self.assertTemplateUsed(response, 'register.html')

  def test_title_register_ada(self):
    response = Client().get('/accounts/register/')
    html_kembalian = response.content.decode('utf8')
    self.assertIn("Sign Up", html_kembalian)
    self.assertTemplateUsed(response, 'register.html')

class UnitTestRegisterLogin(TestCase):

    # Url testcases
    def test_url_is_exist_register(self):
        response = Client().get('/accounts/register/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_is_exist_login(self):
        response = Client().get('/accounts/login/')
        self.assertEqual(response.status_code, 200)

    # View testcases
    def test_view_epicmed_is_using_function_registerPage(self):
        found = resolve('/accounts/register/')
        self.assertEqual(found.func, registerPage)

    def test_view_epicmed_is_using_function_loginPage(self):
        found = resolve('/accounts/login/')
        self.assertEqual(found.func, loginPage)

    # Template testcases
    def test_template_register_using_register_template(self):
        response = Client().get('/accounts/register/')
        self.assertTemplateUsed(response, 'register.html') 

    def test_template_login_using_login_template(self):
        response = Client().get('/accounts/login/')
        self.assertTemplateUsed(response, 'login.html')

class SetUp_Class(TestCase):

    def setUp(self):
        self.user = User.objects.create(username="username", email="user@mp.com", password1="halouser", password2="halouser")


class User_Form_Test(TestCase):
       
    # Valid Form Data
    def test_UserForm_valid(self):
        form = CreateUserForm(data={'username':"username", 'email': "user@mp.com", 'password1': "halouser", 'password2': "halouser"})
        self.assertTrue(form.is_valid())

    # Invalid Form Data
    def test_UserForm_invalid(self):
        form = CreateUserForm(data={'username':"username", 'email': "user@mail.com", 'password1': "halo", 'password2': "halouser"})
        self.assertFalse(form.is_valid())
    
    def test_POST_form(self):
        data = {
            'username':"username", 
            'email': "user@mp.com", 
            'password1': "halouser", 
            'password2': "halouser"
        }

        response = Client().post('/', data)

    def test_view_dalam_template_profile(self):
        user = User.objects.create_user(username='Ryan', password='Pacilkom123')
        user.save()
        self.client.login(username='Ryan', password='Pacilkom123')
        
    
    def test_url_LogOut_if_not_login(self):
       request = Client().get('/accounts/logout/')
       self.assertEqual(request.status_code, 302)

    def test_logout(self):
        self.client.logout()
        # cek sessionnya
        session = self.client.session
        self.assertFalse('user_id' in session)






 